/* s29 index.js */

// this require("express") allows devs to load/import express package that will be used for 
	//the application 
let express = require("express");

// express() - allows devs to create an application using express
const app = express(); // this code creates an express application & stores it inside the "app"
	//variable. Thus, "app" is the server already

const port = 3000;


// use function lets the middleware to do common services & capabilities to the applications
	//
app.use(express.json()); // lets the app to read json data
app.use(express.urlencoded({ extended:true})); //allows the app to receive data fr the forms
// not using these will result to our req.body to return undefined



//Express has methods corresponding to each http methods (get, post, put, delete, etc.)
//this "/" route expects to receive a get rqst at its endpoint (http://localhost:3000/)
app.get("/", (req, res) => {
	// res.send  - allows sending of messages as responses to the client
	res.send("Hello World")
	
})



	// miniactivity
	// 	create a "/hello" route that will receive a GET rqst & will send a message to the 
	// 		client
	// 		message = "hello from the /hello endpoint"
	// 		send the screenshot of your output in the batch google chat
	// rerun server:
	// 	marizzehaideesalanga@Marizzes-MacBook-Pro discussion % node index.js
	// Server is running at port 3000		


app.get("/hello", (req, res) => {
	res.send("hello from the /hello endpoint")
})

// this "/hello" route is expected to receive a post rqst that has json data in the body
app.post("/hello", (req, res) => {
	// to check if the json keys in the body has values
	console.log(req.body.firstName);
	console.log(req.body.lastName);
	// sends the response once the req.body.firstName & req.body.lastName has values
	//	
	// marizzehaideesalanga@Marizzes-MacBook-Pro discussion % node index.js
	// Server is running at port 3000
	// John
	// Doe
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}`) //appears on postman
})
// rerun in terminal, then proceed in postman
// POST
// http://localhost:3000/hello
// body - raw - json
// {
// 
// error message will appear on both postman & terminal
// TypeError: Cannot read properties of undefined
// add code after const port=3000; line 16
// rerun server in gitbash & send again in postman
// output: Hello there John Doe --> working now due to use function

// ===================================================

// GET
// http://localhost:3000/signup

// {
// 	"un":"john"
// 	"pw":"doe"
// }
// expected output:
// user john successfully registered
// ===================================================


// ===================================================
// ===================================================
// ***************  s29 Activity *********************

//1. Create a GET route that will access the "/home" route that will print out a simple 
	//message. 
//2. Process a GET request at the "/home" route using postman. 

app.get("/home", (req, res) => {
	res.send("Welcome to the Home Page!")
})
// http://localhost:3000/home




// 3. Create a GET route that will access the "/users" route that will retrieve all the users 
	//in the mock database. 
// 4. Process a GET request at the "/users" route using postman. 

app.get("/users", (req, res) => {
	console.log(req.body.userName);
	console.log(req.body.passWord);
	res.send(req.body);

})

// http://localhost:3000/users
// {
// 	"userName":"donotdisturb",
// 	"passWord":"sleeping12345"
// }



// 5. Create a DELETE route that will access the "/delete-user" route to remove a user from 
	//the mock database. 
// 6. Process a DELETE request at the "/delete-user" route using postman. 

app.delete("/delete-user", (req, res) => {
	console.log(req.body.userName);
	res.send(`User ${req.body.userName} has been deleted.`);
})

// http://localhost:3000/delete-user
// {
//     "userName": "donotdisturb"
// }



// -create a users variable that will accept an array of objects
// -create /signup route that will accept post method.
// -once the username and the password in the body is filled with information, push the object 
	// into the users array and print the message of "username has registered successfully", 
	// otherwise, send "please input both username and password"
let users = [];

app.post("/signup", (req, res) => {
	
	console.log(req.body.userName);
	console.log(req.body.passWord);
	users.push(req.body);

    if(req.body.userName !== "" && req.body.passWord !== ""){
        res.send(`${req.body.userName} has registered successfully`)
    }else {
        res.send("please input both username and password")
    }
})

// http://localhost:3000/signup
// {
// 	"userName":"donotdisturb",
// 	"passWord":"sleeping12345"
// }
// {
// 	"userName":"lifesabeach",
// 	"passWord":"surfing12345"
// }
// ===================================================
// ===================================================

app.listen(port, () => console.log(`Server is running at port ${port}`));  










